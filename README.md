## :octocat: Pokémon Finder

Application to view types of Pokémons

<br>

## :wrench: Tools and standards adopted
[React](https://reactjs.org/) | [Hooks](https://pt-br.reactjs.org/docs/hooks-intro.html) | [Typescript](https://www.typescriptlang.org/) | [styled-components](https://styled-components.com/) | Mobile First

<br>

## :computer:  Instructions for starting the application

After cloning the repository and accessing the folder via terminal, install the dependencies with the command

```
yarn
```

Then run the application with

```
yarn start
```

<br>

