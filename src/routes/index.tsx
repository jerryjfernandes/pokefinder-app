import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";
import { Home } from "../pages/Home";
import { NewUser } from "../pages/NewUser";
import { FavoriteType } from "../pages/Registration/FavoriteType";
import { Name } from "../pages/Registration/Name";


export default function Routes() {
  return (
    <Router>
      <Switch>
          <Route path="/new-user" exact>
              <NewUser />
          </Route>
          <Route path="/register-favorite-type" exact>
              <FavoriteType />
          </Route>
          <Route path="/register-name" exact>
              <Name />
          </Route>
          <Route path="/" exact>
              <Home />
          </Route>
        </Switch>
    </Router>
  );
}
