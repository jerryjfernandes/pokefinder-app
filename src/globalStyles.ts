import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`

  html, body, #root {
      height: 100vh;
      font-family: 'Open Sans', sans-serif;
      font-style: normal;
      font-weight: 400;
  }
  
  button {
    cursor: pointer;
  }

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }
`;
