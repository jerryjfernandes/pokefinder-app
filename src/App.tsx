import { PokeProvider } from "./hooks/PokeContext";
import Routes from "./routes";
import GlobalStyles from './globalStyles';

export function App() {
  return (
    <PokeProvider>
      <Routes />
      <GlobalStyles />
    </PokeProvider>
  );
}

