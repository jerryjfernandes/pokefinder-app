import { useMemo, useState } from "react";
import { usePokemons } from "../../hooks/PokeContext"
import { Pokemon } from "../Pokemon";

import * as S from './styles';

interface ListProps {
    search: string;
    favoriteType: string;
}

export function List ({search, favoriteType}: ListProps) {
    const { pokemons } = usePokemons();
    const [sort, setSort] = useState('asc');

    const filteredAndSortPokemons = useMemo(() => {
        let pokesFiltered = pokemons.filter((pokemon: Pokemon) => {
            if(pokemon.type.indexOf(favoriteType) !== -1) {
                return pokemon
            }
            return false;
        }).filter(pokemon => {
            if(pokemon.name.toLowerCase().includes(search.toLowerCase())) {
                return pokemon
            }
            return false
        }).filter((p,i,a)=>a.findIndex(v=>(v.id === p.id))===i);

        if (sort === 'desc') {
            return pokesFiltered.sort((a: Pokemon, b: Pokemon) => a.name < b.name ? 1 : -1)
        } else {
            return pokesFiltered.sort((a: Pokemon, b: Pokemon) => a.name > b.name ? 1 : -1)
        }
    },[favoriteType, pokemons, sort, search]);

    return (
        <S.Wrapper>
            <S.TopBar>
                <span>Pokémon</span>
                <div>
                    <button 
                        type="button" 
                        onClick={() => setSort(`${sort === 'asc' ? 'desc' : 'asc'}`)}
                    >
                        Name
                    </button>
                    <img 
                        alt="Arrow" 
                        id="arrow" 
                        style={{transform: `${sort === 'asc' ? 'rotate(180deg)' : 'initial'}`}}
                    />
                </div>
            </S.TopBar>
            <S.Row />
            <S.Main>
                {filteredAndSortPokemons && filteredAndSortPokemons.map((pokemon: Pokemon, index: number) => {
                    return (
                        <div key={index}>
                            <Pokemon 
                                image={pokemon.thumbnailImage}
                                altText={pokemon.thumbnailAltText}
                                name={pokemon.name}
                            />
                            <S.Row />
                        </div>
                    )
                })}
            </S.Main>
        </S.Wrapper>
    )
}