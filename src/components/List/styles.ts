import styled from "styled-components";
import Arrow from '../../assets/arrow.png'

export const Wrapper = styled.div`
    width: 100%;
    margin-top: 0.5rem;
`

export const TopBar = styled.header`
    width: 100%;
    display: flex;
    justify-content: space-between;
    font-size: 0.8rem;

    span {
        margin-left: 0.2rem;

        @media (min-width: 480px) {
            margin-left: 0.5rem;
        }
    }

    div { 
        button {
            border: none;
            background: none;

            @media (min-width: 480px) {
                font-size: 1.2rem;
            }
        }

        img#arrow { 
            content: url(${Arrow});
            margin: 0 0.5rem;
        }
    }

    @media (min-width: 480px) {
        font-size: 1.2rem;
    }
`

export const Row = styled.hr`
    border-top: 0.5px solid #e0dfde;
    margin: 0.1rem 0;
`

export const Main = styled.main`
    width: 100%;
`
