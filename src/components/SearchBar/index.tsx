import { Dispatch, SetStateAction } from "react"
import SearchIcon from '../../assets/search-solid.svg';

import * as S from './styles';
interface SearchBarProps {
    search: string;
    setSearch: Dispatch<SetStateAction<string>>;
}
export function SearchBar({search, setSearch}: SearchBarProps) {
    return (
        <S.Wrapper>
            <input
                value={search}
                name="search"
                onChange={e => setSearch(e.target.value)}
                placeholder="Pokemon Finder"
            />

            <img src={SearchIcon} alt="" />
        </S.Wrapper>
    )
}