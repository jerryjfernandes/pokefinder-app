import styled from "styled-components";

export const Wrapper = styled.div`
    width: 100%;
    height: 3rem;
    display: flex;
    justify-content: center;
    align-items: center;
    background: #19C7A1;

    input {
        width: 90%;
        padding: 0.5rem;

    }

    img {
        width: 0.8rem;
        position: absolute;
        right: 6.5%;
    }
`