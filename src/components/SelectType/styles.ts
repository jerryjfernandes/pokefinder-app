import styled from "styled-components";

export const Wrapper = styled.div`
    width: 100%;
    height: 6rem;
    display: flex;
    overflow-x: auto;
    margin-top: 1rem;
`

export const PokeType = styled.button`
    display: flex;
    flex-direction: column;
    align-items: center;
    background: none;
    border: none;
    width: 8rem;
    margin: 0 0.3rem;

    img {
        width: 4rem;
        height: 4rem;
        border-radius: 50%;
    }
    span {
        font-weight: 700;
    }
`