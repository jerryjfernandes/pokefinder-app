import { Dispatch, SetStateAction, useCallback } from "react";
import { usePokemons } from "../../hooks/PokeContext"

import * as S from './styles';

interface SelectTypeProps {
    setFavoriteType: Dispatch<SetStateAction<string>>;
}

export function SelectType({setFavoriteType}: SelectTypeProps) {
    const {pokeTypes} = usePokemons();

    const setPokeType = useCallback((favoriteType: string) => {
        window.localStorage.setItem('favoriteType', favoriteType);
        setFavoriteType(favoriteType);
    },[setFavoriteType])

    return (
        <S.Wrapper>
            {pokeTypes && pokeTypes.map((type: PokeType, index: number) => {
                return (
                    <S.PokeType key={index} type="button" onClick={() => setPokeType(type.name)}>
                        <img src={type.thumbnailImage} alt={`Pokémon type ${type.name}`} />
                        <span>{type.name.charAt(0).toUpperCase() + type.name.slice(1)}</span>
                    </S.PokeType>
                )
            })}
        </S.Wrapper>
    )
}