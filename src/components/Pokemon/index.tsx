import * as S from './styles';

interface PokemonProps {
    image: string;
    altText: string;
    name: string;
}

export function Pokemon({image, altText, name}: PokemonProps) {
    return (
        <S.Wrapper>
            <div>
                <img src={image} alt={altText} />   
            </div>
            <p>{name}</p>
        </S.Wrapper>
    )
}