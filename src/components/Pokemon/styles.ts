import styled from "styled-components";

export const Wrapper = styled.div`
    width: 100%;
    display: flex;
    align-items: center;

    div {
        width: 5rem;
        height: 5rem;

        img {
            width: 5rem;
            height: 5rem;
        }
    }

    p {
        margin-left: 1rem;
    }
`