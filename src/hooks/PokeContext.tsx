import {
    createContext,
    Dispatch,
    ReactNode,
    SetStateAction,
    useContext,
    useState,
  } from 'react';
  
  type ContextType = {
    pokeTypes: any[];
    setPokeTypes: Dispatch<SetStateAction<PokeType[]>>;
    pokemons: any[];
    setPokemons: Dispatch<SetStateAction<Pokemon[]>>;
  };
  
  interface PokeProviderProps {
    children: ReactNode;
  }
  
  const PokeContext = createContext({} as ContextType);
  
  export const PokeProvider = ({
    children,
  }: PokeProviderProps) => {
    const [pokeTypes, setPokeTypes] = useState<PokeType[]>([]);
    const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  
    return (
      <PokeContext.Provider
        value={{pokeTypes, setPokeTypes, pokemons, setPokemons}}
      >
        {children}
      </PokeContext.Provider>
    );
  };
  
  export const usePokemons = (): ContextType => {
    return useContext(PokeContext);
  };
  