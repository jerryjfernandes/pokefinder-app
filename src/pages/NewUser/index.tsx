import { Link } from "react-router-dom";

import * as S from './styles';

export function NewUser () {
    return (
        <S.Wrapper>
            <S.Header>
                <img alt="Pokémon logo" id="logo" />
                <img alt="Finder" id="finder" />
            </S.Header>
            <S.Main>
                <Link to="/register-name">
                    <button type="button">Let's go!</button>
                </Link>
            </S.Main>
            <S.Footer>
                <img alt="Pikachu" id="pikachu" />
            </S.Footer>
        </S.Wrapper>
    )
}