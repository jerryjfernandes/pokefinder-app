import styled from "styled-components";
import BackgroundImage from '../../assets/bg.png'
import BackgroundImageTablet from '../../assets/bg@2x.png'
import BackgroundImagePc from '../../assets/bg@3x.png'
import PokemonLogo from '../../assets/pokemon-logo.png'
import PokemonLogoTablet from '../../assets/pokemon-logo@2x.png'
import PokemonLogoPc from '../../assets/pokemon-logo@3x.png'
import Finder from '../../assets/finder.png'
import FinderTablet from '../../assets/finder@2x.png'
import FinderPc from '../../assets/finder@3x.png'
import Pikachu from '../../assets/pikachu.png'
import PikachuTablet from '../../assets/pikachu@2x.png'
import PikachuPc from '../../assets/pikachu@3x.png'


export const Wrapper = styled.div`
    width: 100%;
    height: 100%;
    background-image: url(${BackgroundImage});
    background-size: cover;
    background-position: center;
    position: absolute;

    @media (min-width: 480px) {
        background-image: url(${BackgroundImageTablet});
    }

    @media (min-width: 769px) {
        background-image: url(${BackgroundImagePc});
    }

`

export const Header = styled.div`
    width: 100%;
    height: 30vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 3rem;

    img#logo { 
        content: url(${PokemonLogo});

        @media (min-width: 480px) {
            content: url(${PokemonLogoTablet});
        }

        @media (min-width: 769px) {
            content: url(${PokemonLogoPc});
            width: 40%;
        }
    }

    img#finder { 
        content: url(${Finder});
        
        @media (min-width: 480px) {
            content: url(${FinderTablet});
        }

        @media (min-width: 769px) {
            content: url(${FinderPc});
            width: 15%;
        }
    }
`

export const Main = styled.main`
    width: 100%;
    height: 40%;
    display: flex;
    justify-content: center;
    align-items: center;

    button {
        width: 15rem;
        height: 2.5rem;
        border-radius: 0.5rem;
        background: #f01f76;
        border: none;
        color: #f5f5f5;
        font-size: 1.3rem;

        :hover {
            opacity: 0.9;
        }

        @media (min-width: 480px) {
            width: 20rem;
            height: 3.5rem;
        }
    }
`

export const Footer = styled.footer`
    width: 100%;

    img#pikachu { 
        content: url(${Pikachu});
        position: absolute;
        right: 0;
        bottom: 0;

        @media (min-width: 480px) {
            content: url(${PikachuTablet});
        }

        @media (min-width: 769px) {
            content: url(${PikachuPc});
            width: 35%;
        }
    }
`