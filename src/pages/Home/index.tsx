import { useCallback, useEffect, useState } from "react";
import { useHistory } from "react-router";
import { List } from "../../components/List";
import { SearchBar } from "../../components/SearchBar";
import { SelectType } from "../../components/SelectType";
import { usePokemons } from "../../hooks/PokeContext";

import * as S from './styles';

export function Home () {
    const history = useHistory();
    const { pokeTypes, setPokeTypes, pokemons, setPokemons } = usePokemons();
    const [favoriteType, setFavoriteType] = useState('normal')
    const [search, setSearch] = useState('');

    const verifyUser = useCallback(() => {
        const user = window.localStorage.getItem('user');
        if (!user) {
            history.push('/new-user')
        }
    }, [history]);

    const verifyFavoriteType = useCallback(() => {
        const user = window.localStorage.getItem('user');
        const info = window.localStorage.getItem('favoriteType');
        if (!user) {
            history.push('/new-user')
        } else if (!info) {
            history.push('/register-favorite-type')
        } else {
            setFavoriteType(info)
        }
    }, [history]);

    const getPokemons = useCallback(async () => {
        await fetch('/files/pokemon/data/pokemons.json')
            .then(response => response.json())
            .then(data => setPokemons(data))
    },[setPokemons]);

    const getPokeTypes = useCallback(async () => {
        await fetch('/files/pokemon/data/types.json')
            .then(response => response.json())
            .then(data => setPokeTypes(data.results))
    },[setPokeTypes])

    useEffect(() => {
        verifyUser();
        verifyFavoriteType();
        if(!pokemons.length) {
            getPokemons();
        }
        if(!pokeTypes.length) {
            getPokeTypes();
        }
    },[
        verifyUser,
        verifyFavoriteType, 
        getPokemons, 
        getPokeTypes, 
        pokeTypes, 
        pokemons])
    
    return (
        <S.Wrapper>
            <SearchBar search={search} setSearch={setSearch} />
            <SelectType setFavoriteType={setFavoriteType}/>
            <List search={search} favoriteType={favoriteType} />
        </S.Wrapper>
    )
}