import styled from "styled-components";
import BackgroundImage from '../../../assets/bg.png'
import BackgroundImageTablet from '../../../assets/bg@2x.png'
import BackgroundImagePc from '../../../assets/bg@3x.png'
import Next from '../../../assets/next.png'
import NextTablet from '../../../assets/next@2x.png'
import NextPc from '../../../assets/next@3x.png'

export const Wrapper = styled.div`
    width: 100%;
    height: 100%;
    background-image: url(${BackgroundImage});
    background-size: cover;
    background-position: center;
    position: absolute;
    color: #f5f5f5;

    header {
        width: 100%;
        height: 30vh;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 1.4rem;
    }

    @media (min-width: 480px) {
        background-image: url(${BackgroundImageTablet});

        header {
            font-size: 1.8rem;
        }
    }

    @media (min-width: 769px) {
        background-image: url(${BackgroundImagePc});
    }

`

export const Main = styled.main`
    width: 100%;
    height: 60%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-size: 0.9rem;

    form {
        width: 90%;
        margin-top: 3rem;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        align-items: center;

        select {
            width: 100%;
            border: none;
            background: none;
            border-bottom: 0.2rem solid #f5f5f5;
            color: #f5f5f5;
            font-size: 2rem;
            text-align: center;

            @media (min-width: 480px) {
                width: 50%;
            }

            @media (min-width: 769px) {
                width: 30%;
            }
        }

        button {
            border: none;
            background: none;

            img#next-step { 
                content: url(${Next});

                @media (min-width: 480px) {
                    content: url(${NextTablet});
                }

                @media (min-width: 769px) {
                    content: url(${NextPc});
                    width: 40%;
                }
            }
        }
    }

    @media (min-width: 480px) {
        font-size: 1.2rem;
    }
`