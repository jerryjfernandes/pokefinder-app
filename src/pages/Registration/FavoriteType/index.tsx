import { ChangeEvent, useCallback, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { usePokemons } from "../../../hooks/PokeContext";

import * as S from './styles';

export function FavoriteType () {
    const history = useHistory();
    const { pokeTypes, setPokeTypes } = usePokemons()
    const [favoriteType, setFavoriteType] = useState('normal');

    const setFavType = useCallback(() => {
        window.localStorage.setItem('favoriteType', favoriteType);
    }, [favoriteType]);

    const handleSubmit = useCallback(async (e: ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        setFavType()
        history.push('/')
    }, [history, setFavType]);

    const getPokeTypes = useCallback(async () => {
        await fetch('/files/pokemon/data/types.json')
            .then(response => response.json())
            .then(data => setPokeTypes(data.results))
    },[setPokeTypes])

    useEffect(() => {
        const user = window.localStorage.getItem('user');
        if (!user) {
            history.push('/new-user')
        }
        getPokeTypes();
    },[history, getPokeTypes])

    return (
        <S.Wrapper>
            <header>
                <p>Hello, trainer Dev!</p>
            </header>
            <S.Main>
                <p>...now tell us wich is your favorite Pokémon type:</p>
                <form onSubmit={handleSubmit}>
                    <select onChange={e => setFavoriteType(e.target.value)}>
                        {pokeTypes && pokeTypes.map((type: PokeType, index: number) => {
                            return (
                                <option key={index} value={type.name}>
                                    {type.name.charAt(0).toUpperCase() + type.name.slice(1)}
                                </option>
                            )
                        })}
                    </select>
                    <div>
                        <button 
                            type="submit" 
                        >
                            <img alt="Go to next step" id="next-step" />
                        </button>
                    </div>
                </form>
            </S.Main>
        </S.Wrapper>
    )
}
