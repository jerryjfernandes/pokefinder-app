import { ChangeEvent, useCallback, useState } from "react";
import { useHistory } from "react-router-dom";

import * as S from './styles';

export function Name () {
    const history = useHistory();
    const [name, setName] = useState('');

    const setUser = useCallback(() => {
        window.localStorage.setItem('user', name);
    }, [name]);

    const handleSubmit = useCallback(async (e: ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        setUser()
        history.push('/register-favorite-type')
    }, [history, setUser]);

    return (
        <S.Wrapper>
            <header>
                <p>Let's meet each other first?</p>
            </header>
            <S.Main>
                <p>First we need to know your name...</p>
                <form onSubmit={handleSubmit}>
                    <input 
                        type="text"
                        value={name} 
                        onChange={e => setName(e.target.value)} 
                        required 
                    />
                    <div>
                        <button 
                            type="submit" 
                        >
                            <img alt="Go to next step" id="next-step" />
                        </button>
                    </div>
                </form>
            </S.Main>
        </S.Wrapper>
    )
}